#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#include "print_maze.h"
#include "random_direction.h"



/* Chooses a direction at random using the function random_direction() 
 * and checks if it is a valid cell for visiting 
 * If so, it recursively calls the same function with the new coordinates 
 */
void dfs_maze(char **mat, int m, int n, int x, int y){
	//TODO
}


int main ()
{
	int i, rows, cols, start_x, start_y, m, n, x, y;
	char **maze;

	srand(time(0));
	
	printf("Insert the dimensions of the maze and the starting location (start location (0,0) - (m-1, n-1) \n");
	scanf("%d %d %d %d", &m, &n, &x, &y);

	while((m <= 0) || (n <= 0) || (x >= m) || (y >= n) || (x < 0) || (y < 0))
	{
		printf("Wrong input. Insert positive integers for the size of the maze and start location values between (0,0) and (m-1, n-1) \n");
		scanf("%d %d %d %d", &m, &n, &x, &y);
	}
	
	rows = 2 * m + 1;
	cols = 2 * n + 1;
	start_x = 2 * x + 1;
	start_y = 2 * y + 1;
	
	//TODO

	return 0;
  }
