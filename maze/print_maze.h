/* Header zum vierten Aufgabenblatt: Konstruktion eines Labyrinths
 * Einfuehrung in die Programmierung I
 * TU Darmstadt, Wintersemester 20/21
 * Ausgabe des Labyrinths
 */

#ifndef PRINT_MAZE_H
#define PRINT_MAZE_H

/* Gibt eine Matrix der Hoehe height (2*m + 1) und Breite width (2*n + 1)
 * mit den in der Konfiguration festgelegten
 * Zeichen und Farben aus
*/
void print_maze(char ** maze, int height, int width);

#endif
